#ifndef __CLOSESTPOINTQUERY_H__
#define __CLOSESTPOINTQUERY_H__

#include "Mesh.h"
#include "SimpleBVH.h"

namespace Util
{

/*!
 Class for getting closest point on a mesh. \n
 IMPORTANT the mesh is assumed to exist and static while in use.
 Instantiating is slow. Query is fast and threadsafe.
*/
class ClosestPointQuery
{
public:
    /*! Constructor
      \param[in] m mesh to test. 
      */
    ClosestPointQuery(const Mesh &m)
    : mesh(m)
    {
        // build tree
        tree = new SimpleBVH(mesh);
    }

    ~ClosestPointQuery()
    {
        delete tree;
    }

    /*! Return the closest point on the mesh within the specified maximum 
        search distance.
      \param[out] result where output gets stored
      \param[in] queryPoint query point
      \param[in] maxDist maximum search radius
      \return true if point found. false otherwise.
      */
    bool operator() (glm::vec3 *result, 
        const glm::vec3 &queryPoint, float maxDist) const
    {
        if (!tree->getRoot()) return false;
        glm::vec3 candidate;
        float dist2 = maxDist * maxDist;
        if (getClosestRecursive(&candidate, queryPoint, tree->getRoot(), &dist2))
            *result = candidate;
    }

private:

    /**
     \param d2 minimum distance squared keeps track of closest distance so far during search
    */
    bool getClosestRecursive(glm::vec3 *cand, 
        const glm::vec3 &queryPoint, const SimpleBVH::Node *node, float *d2) const
    {
        // if leaf
        if (!node->c0)
        {
            // test every element in the node
            bool res = false;
            for (int i = 0; i < node->tris.size(); ++i)
            {
                const int t = node->tris[i];
                const glm::vec3 &p0 = mesh.verts[mesh.indices[t*3]];
                const glm::vec3 &p1 = mesh.verts[mesh.indices[t*3+1]];
                const glm::vec3 &p2 = mesh.verts[mesh.indices[t*3+2]];

                const glm::vec3 closest = closestOnTri(p0, p1, p2, queryPoint);
                const glm::vec3 toClosest = closest - queryPoint;

                // is the point on triangle closer than previous result?
                if (glm::dot(toClosest, toClosest) < *d2)
                {
                    *d2 = glm::dot(toClosest, toClosest);
                    *cand = closest;
                    res = true;
                }
            }
            return res;
        }
        else // if not leaf
        {
            // early out
            if (!node->test(queryPoint, *d2))
                return false;

            // explore closest child first
            const glm::vec3 center0 = (node->c0->bbmin + node->c0->bbmax) * 0.5f;
            const glm::vec3 center1 = (node->c1->bbmin + node->c1->bbmax) * 0.5f;
            // distance squared to node center is good enough approx
            if (glm::dot(center0, queryPoint) < glm::dot(center1, queryPoint))
            {
                // to avoid short circuit
                bool res = getClosestRecursive(cand, queryPoint, node->c0, d2) |
                           getClosestRecursive(cand, queryPoint, node->c1, d2);
                return res;
            }
            else
            {
                bool res = getClosestRecursive(cand, queryPoint, node->c1, d2) |
                           getClosestRecursive(cand, queryPoint, node->c0, d2);
                return res;
            }
        }
    }
    
    const Mesh &mesh;
    SimpleBVH *tree;
};

}//ns
#endif
