#include "Mesh.h"

namespace Util
{

glm::vec3 closestOnEdge(const glm::vec3 &p0, 
                        const glm::vec3 &p1, 
                        const glm::vec3 &queryPoint)
{
    glm::vec3 dir = p1-p0;
    const float l2 = glm::dot(dir, dir);
    // zero length egde
    if (l2 == 0.0f) return p0;

    const float l = sqrtf(l2);
    dir /= l;
    const float t = glm::dot(dir, queryPoint-p0);
    if (t < 0) return p0;
    if (t > l) return p1;
    
    return p0 + dir * t;
}

glm::vec3 closestOnTri(const glm::vec3 &p0, 
                       const glm::vec3 &p1, 
                       const glm::vec3 &p2, 
                       const glm::vec3 &queryPoint)
{
    const glm::vec3 v0 = p1 - p0; 
    const glm::vec3 v1 = p2 - p0; 

    glm::vec3 n = glm::cross(v0, v1);
    if (glm::dot(n, n) != 0.0f)
    {
        n = glm::normalize(n);
        // point on the plane
        glm::vec3 onPlane = queryPoint - n * glm::dot(n, queryPoint-p0);
        const glm::vec3 v2 = onPlane - p0;

        // bary center test
        const float d00 = glm::dot(v0, v0);
        const float d01 = glm::dot(v0, v1);
        const float d11 = glm::dot(v1, v1);
        const float d20 = glm::dot(v2, v0);
        const float d21 = glm::dot(v2, v1);
        const float denom = d00 * d11 - d01 * d01;
        const float v = (d11 * d20 - d01 * d21) / denom;
        const float w = (d00 * d21 - d01 * d20) / denom;
        if ((v >= 0) && (w >= 0) && (v + w < 1))
            return onPlane;
    }

    // edge test
    const glm::vec3 ep0 = closestOnEdge(p0, p1, queryPoint);
    const glm::vec3 ep1 = closestOnEdge(p1, p2, queryPoint);
    const glm::vec3 ep2 = closestOnEdge(p2, p0, queryPoint);
    // dist^2 for each edge
    const float d0 = glm::dot(ep0-queryPoint, ep0-queryPoint);
    const float d1 = glm::dot(ep1-queryPoint, ep1-queryPoint);
    const float d2 = glm::dot(ep2-queryPoint, ep2-queryPoint);
    if (d0 <= d1 && d0 <= d2)
        return ep0;
    else if (d1 <= d0 && d1 <= d2)
        return ep1;
    else
        return ep2;
}

}//ns
