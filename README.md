Just an exercise (juhyundaniel@gmail.com)

Implementation of a class that helps you find the closest point on a mesh.

Util::ClosestPointQuery is the main interface and contains the core of the algorithm. 
Mesh.h has simple mesh container, and I also snuck in some helper functions in there.

test directory contains unit tests, and also shows you how make queries.

Design Notes:
 - pointOnTriangle test might be suboptimal. There might be better algorithms out there (I haven't checked).
 - SimpleBVH is not a very efficient tree. I didn't bother optimizing it.

Assumptions:
 - the mesh is assumed to exist and static for any instance of ClosestPointQuery.
 - single precision only.

External libraries used:
 - glm for vector type