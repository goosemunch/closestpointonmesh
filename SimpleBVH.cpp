#include "SimpleBVH.h"

namespace Util
{

void SimpleBVH::buildBVH(const Mesh &m, Node *node, const std::vector<int> &xlist)
{
    // take list of triangles
    // build bounding box
    computeBBox(&node->bbmin, &node->bbmax, m, &xlist[0], xlist.size());

    if (xlist.size() > maxLeafSize)
    {
        // find longest axis
        float axisLen = node->bbmax.x - node->bbmin.x;
        int axis = 0;
        if (node->bbmax.y - node->bbmin.y > axisLen)
        {
            axisLen = node->bbmax.y - node->bbmin.y;
            axis = 1;
        }
        if (node->bbmax.z - node->bbmin.z > axisLen)
        {
            axisLen = node->bbmax.z - node->bbmin.z;
            axis = 2;
        }

        std::vector<int> leftlist, rightlist;
        const float midpoint = (node->bbmax[axis] + node->bbmin[axis]) * 0.5;
        for (int i = 0; i < xlist.size(); ++i)
        {
            const int tri = xlist[i];
            const glm::vec3 &v0 = m.verts[m.indices[tri * 3]];
            const glm::vec3 &v1 = m.verts[m.indices[tri * 3 + 1]];
            const glm::vec3 &v2 = m.verts[m.indices[tri * 3 + 2]];
            const float centroid = (v0[axis] + v1[axis] + v2[axis]) / 3.0f;

            if (centroid < midpoint) leftlist.push_back(tri);
            else rightlist.push_back(tri);
        }

        if (leftlist.empty() || rightlist.empty()) 
        {
            // something went wrong (overlapping tris or something)
            node->tris = xlist;
        }
        else
        {
            node->c0 = new Node;
            node->c1 = new Node;
            buildBVH(m, node->c0, leftlist);
            buildBVH(m, node->c1, rightlist);
        }
    }
    else
    {
        node->tris = xlist;
    }
}

void SimpleBVH::computeBBox(glm::vec3 *bbmin, glm::vec3 *bbmax, 
     const Mesh &m, const int *list, const int listSize)
{
    *bbmin = glm::vec3(FLT_MAX);
    *bbmax = glm::vec3(-FLT_MAX);

    for (int ctr = 0; ctr < listSize; ctr++)
    {
        const int tri = list[ctr];
        const glm::vec3 *v[3];
        v[0] = &m.verts[m.indices[tri * 3]];
        v[1] = &m.verts[m.indices[tri * 3 + 1]];
        v[2] = &m.verts[m.indices[tri * 3 + 2]];
        for (int j = 0; j < 3; j++)
        {
            // expand bbox
            if (v[j]->x < bbmin->x) bbmin->x = v[j]->x;
            if (v[j]->y < bbmin->y) bbmin->y = v[j]->y;
            if (v[j]->z < bbmin->z) bbmin->z = v[j]->z;
            if (v[j]->x > bbmax->x) bbmax->x = v[j]->x;
            if (v[j]->y > bbmax->y) bbmax->y = v[j]->y;
            if (v[j]->z > bbmax->z) bbmax->z = v[j]->z;
        }
    }
}

}//ns
