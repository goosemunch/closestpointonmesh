#ifndef __MESH_H__
#define __MESH_H__

#include <glm/glm.hpp>
#include <vector>

namespace Util
{

/*!
 Simple mesh made of triangles
*/
struct Mesh
{
    /*! vertex positions */
    std::vector<glm::vec3> verts;
    /*! indices (# of tris * 3) */
    std::vector<int> indices;
};

/*!
 Given a line segment and a point, return the closest point on the line.
 \param[in] p0 line begin
 \param[in] p1 line end
 \param[in] queryPoint test point
 \return result
*/
glm::vec3 closestOnEdge(const glm::vec3 &p0, 
                        const glm::vec3 &p1, 
                        const glm::vec3 &queryPoint);

/*!
 Given a triangle and a point, return the closest point on the triangle.
 Handles degenerates (zero area, zero edge length cases). Could be faster.
 \param[in] p0 triangle p0
 \param[in] p1 triangle p1
 \param[in] p1 triangle p2
 \param[in] queryPoint test point
 \return result
*/
glm::vec3 closestOnTri(const glm::vec3 &p0, 
                       const glm::vec3 &p1, 
                       const glm::vec3 &p2, 
                       const glm::vec3 &queryPoint);

}//ns
#endif
