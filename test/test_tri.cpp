#include "../Mesh.h"

#define DISSIMILAR(x,y) (glm::dot(x-y, x-y) > 1e-5)
#define ERROUT(x) \
{ \
    fprintf(stderr, x); \
    return 1; \
}

// testing closestOnEdge
int test0()
{
    glm::vec3 e0, e1, p, res;
    
    // case: zero length edge
    e0 = e1 = glm::vec3(1,1,1);
    p = glm::vec3(0,0,0);
    res = Util::closestOnEdge(e0, e1, p);
    if (DISSIMILAR(res,e0))
        ERROUT("closestOnEdge degenerate case failed\n");

    // case: projected point is before line segment
    e0 = glm::vec3(-1, 0, 0);
    e1 = glm::vec3(1, 0, 0);
    p = glm::vec3(-2, 1, 0);
    res = Util::closestOnEdge(e0, e1, p);
    if (DISSIMILAR(res,e0))
        ERROUT("closestOnEdge outside case 1 failed\n");

    // case: projected point is after line segment
    p = glm::vec3(2, 1, 0);
    res = Util::closestOnEdge(e0, e1, p);
    if (DISSIMILAR(res,e1))
        ERROUT("closestOnEdge outside case 2 failed\n");

    // case: normal
    p = glm::vec3(0, 1, 0);
    res = Util::closestOnEdge(e0, e1, p);
    if (DISSIMILAR(res,glm::vec3(0,0,0)))
        ERROUT("closestOnEdge normal case failed\n");

    return 0;
}

// testing closestOnTri
int test1()
{
    glm::vec3 p0, p1, p2, p, res;
    
    // case: zero area triangle
    // still need to return point along edge/points
    p0 = glm::vec3(-1, 0, 0);
    p1 = glm::vec3(2, 0, 0);
    p2 = glm::vec3(0, 0, 0);
    p = glm::vec3(1, 1, 0);
    res = Util::closestOnTri(p0, p1, p2, p);
    if (DISSIMILAR(res, glm::vec3(1, 0, 0)))
        ERROUT("closestOnTri zero area tri failed\n");

    // case: one of the edges of the triangle is zero length
    // still need to return point along edge
    p2 = glm::vec3(2, 0, 0);
    p = glm::vec3(1, 1, 0);
    res = Util::closestOnTri(p0, p1, p2, p);
    if (DISSIMILAR(res, glm::vec3(1, 0, 0)))
        ERROUT("closestOnTri zero edge length tri failed\n");

    // case: normal. closest point is on a vert
    p0 = glm::vec3(0, 0, 0);
    p1 = glm::vec3(2, 2, 0);
    p2 = glm::vec3(-2, 2, 0);
    p = glm::vec3(10, 10, 1);
    res = Util::closestOnTri(p0, p1, p2, p);
    if (DISSIMILAR(res, p1))
        ERROUT("closestOnTri normal vert case failed\n");

    // case: normal. closest point is on an edge
    p = glm::vec3(0, 10, 1);
    res = Util::closestOnTri(p0, p1, p2, p);
    if (DISSIMILAR(res, glm::vec3(0, 2, 0)))
        ERROUT("closestOnTri normal edge case failed\n");

    // case: normal. closest point is inside tri
    p = glm::vec3(0, 1, -10);
    res = Util::closestOnTri(p0, p1, p2, p);
    if (DISSIMILAR(res, glm::vec3(0, 1, 0)))
        ERROUT("closestOnTri normal inside case failed\n");

    return 0;
}

int main(int argc, char*argv[])
{
    int status = 0;

    status = 
        test0() |
        test1()
        ;
    
    return status;
}
