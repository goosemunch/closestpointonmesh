#include "../Mesh.h"
#include "../ClosestPointQuery.h"

#define DISSIMILAR(x,y) (glm::dot(x-y, x-y) > 1e-6)
#define ERROUT(x) \
{ \
    fprintf(stderr, x); \
    return 1; \
}

// empty mesh
int test0()
{
    Util::Mesh mesh;
    Util::ClosestPointQuery closest(mesh);
    glm::vec3 res;
    if (closest(&res, glm::vec3(1, 1, 1), 1.0f))
        ERROUT("ClosestPointQuery empty mesh failed\n");
    return 0;
}

// simpe mesh
int test1()
{
    glm::vec3 res;
    Util::Mesh mesh;
    mesh.verts.push_back(glm::vec3(0, 0, 0));
    mesh.verts.push_back(glm::vec3(1, 0, 1));
    mesh.verts.push_back(glm::vec3(1, 0, -1));
    mesh.verts.push_back(glm::vec3(1, 1, 0));
    mesh.indices.push_back(0); mesh.indices.push_back(1); mesh.indices.push_back(2);
    mesh.indices.push_back(1); mesh.indices.push_back(3); mesh.indices.push_back(2);

    Util::ClosestPointQuery closest(mesh);

    // case: point is further away from the mesh than the test radius
    if (closest(&res, glm::vec3(10, 10, 10), 1.0f))
        ERROUT("ClosestPointQuery distant point failed\n");

    // case: normal on edge
    if (!closest(&res, glm::vec3(2, -2, 0.5), 5.0f))
        ERROUT("ClosestPointQuery normal edge case return failed\n");
    if (DISSIMILAR(res, glm::vec3(1, 0, 0.5)))
        ERROUT("ClosestPointQuery normal edge case value failed\n");

    // case: normal on vertex
    if (!closest(&res, glm::vec3(1, 0.1, -2), 5.0f))
        ERROUT("ClosestPointQuery normal vert case return failed\n");
    if (DISSIMILAR(res, glm::vec3(1, 0, -1)))
        ERROUT("ClosestPointQuery normal vert case value failed\n");

    // case: normal on triangle
    if (!closest(&res, glm::vec3(0.2, 0.5, 0.1), 5.0f))
        ERROUT("ClosestPointQuery normal inside 1 return failed\n");
    if (DISSIMILAR(res, glm::vec3(0.2, 0, 0.1)))
        ERROUT("ClosestPointQuery normal inside 1 value failed\n");

    // case: normal on second triangle
    if (!closest(&res, glm::vec3(1.5, 0.5, 0), 5.0f))
        ERROUT("ClosestPointQuery normal inside 2 return failed\n");
    if (DISSIMILAR(res, glm::vec3(1, 0.5, 0)))
        ERROUT("ClosestPointQuery normal inside 2 value failed\n");

    return 0;
}

// randomly generate mesh, do brute force search, and compare
// result against the tree search
// (mainly to see if the tree is behaving correctly)
int test2()
{
    srand(1234);
    glm::vec3 res;
    Util::Mesh mesh;
    int numTris = 100;
    for (int i = 0; i < numTris; ++i)
    {
        glm::vec3 off(
            ((float)rand() / RAND_MAX - 0.5) * 5.0,
            ((float)rand() / RAND_MAX - 0.5) * 5.0,
            ((float)rand() / RAND_MAX - 0.5) * 5.0
            );
        glm::vec3 a(
            (float)rand() / RAND_MAX - 0.5,
            (float)rand() / RAND_MAX - 0.5,
            (float)rand() / RAND_MAX - 0.5
            );
        glm::vec3 b(
            (float)rand() / RAND_MAX - 0.5,
            (float)rand() / RAND_MAX - 0.5,
            (float)rand() / RAND_MAX - 0.5
            );
        glm::vec3 c(
            (float)rand() / RAND_MAX - 0.5,
            (float)rand() / RAND_MAX - 0.5,
            (float)rand() / RAND_MAX - 0.5
            );
        mesh.verts.push_back(a + off);
        mesh.verts.push_back(b + off);
        mesh.verts.push_back(c + off);
        mesh.indices.push_back(mesh.indices.size());
        mesh.indices.push_back(mesh.indices.size());
        mesh.indices.push_back(mesh.indices.size());
    }

    Util::ClosestPointQuery closest(mesh);

    // compare with brute force test
    int numTests = 50;
    for (int i = 0; i < numTests; ++i)
    {
        glm::vec3 p(
            ((float)rand() / RAND_MAX - 0.5) * 5.0,
            ((float)rand() / RAND_MAX - 0.5) * 5.0,
            ((float)rand() / RAND_MAX - 0.5) * 5.0
            );

        // brute force search
        glm::vec3 bruteResult;
        float mindist2 = FLT_MAX;
        for (int t = 0; t < mesh.indices.size() / 3; ++t)
        {
            const glm::vec3 &p0 = mesh.verts[mesh.indices[t * 3 + 0]];
            const glm::vec3 &p1 = mesh.verts[mesh.indices[t * 3 + 1]];
            const glm::vec3 &p2 = mesh.verts[mesh.indices[t * 3 + 2]];
            glm::vec3 cand = Util::closestOnTri(p0, p1, p2, p);
            float currdist2 = glm::dot(cand - p, cand - p);
            if (currdist2 < mindist2)
            {
                bruteResult = cand;
                mindist2 = currdist2;
            }
        }
        // tree search
        glm::vec3 optimizedResult;
        if (!closest(&optimizedResult, p, 100.0f))
            ERROUT("ClosestPointQuery random return failed\n");

        // compare
        if (DISSIMILAR(optimizedResult, bruteResult))
            ERROUT("ClosestPointQuery random value failed\n");
    }
    
    return 0;
}

int main(int argc, char *argv[])
{
    int status = 0;

    status = 
        test0() |
        test1() |
        test2()
        ;
    
    return status;
}
