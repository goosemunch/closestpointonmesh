#ifndef __SIMPLEBVH_H__
#define __SIMPLEBVH_H__

#include "Mesh.h"
#include <algorithm>

namespace Util
{

/*! Simple Binary BVH. 
 Simply halvig longest axis. Could be optimized with SAH or such.
*/
class SimpleBVH
{
public:
    /*! BVH node type */
    struct Node
    {
        Node()
        : c0(NULL), c1(NULL)
        {
        }

        ~Node()
        {
            delete c0;
            delete c1;
        }

        /*! sphere bb test
        \param[in] p point to test
        \param[in] d2 squared
        \return true if sphere intersects expanded box
        */
        bool test(const glm::vec3 &p, const float d2) const
        {
            // clamp test point to bb
            glm::vec3 q = p;
            std::max(bbmin.x, std::min(bbmax.x, q.x));
            std::max(bbmin.y, std::min(bbmax.y, q.y));
            std::max(bbmin.z, std::min(bbmax.z, q.z));

            // is it inside the box or within test radius?
            return (q.x > bbmin.x && q.x < bbmax.x &&
                    q.y > bbmin.y && q.y < bbmax.y &&
                    q.z > bbmin.z && q.z < bbmax.z) ||
                   glm::dot(p-q,p-q) < d2;
        }

        Node *c0, *c1; // children (NULL if leaf)
        glm::vec3 bbmin, bbmax; // bounding box
        std::vector<int> tris;
    };

    SimpleBVH(const Mesh &m)
    : root(NULL), maxLeafSize(4)
    {
        if (m.indices.empty() || m.verts.empty()) return;

        const int numTris = m.indices.size() / 3;
        std::vector<int> alltris(numTris);
        for (int i = 0; i < numTris; ++i) alltris[i] = i;
       
        root = new Node;
        buildBVH(m, root, alltris);
    }

    ~SimpleBVH()
    {
        delete root;
    }

    const Node* getRoot() const
    {
        return root;
    }

private:
    /*! recursively build */
    void buildBVH(const Mesh &m, Node *node, const std::vector<int> &xlist);

    /*! compute bounding box for a list of triangles */
    void computeBBox(glm::vec3 *bbmin, glm::vec3 *bbmax, 
        const Mesh &m, const int *list, const int listSize);
    
    Node *root;
    int maxLeafSize;
};

}//ns
#endif
